﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using TMPro;

/**
 * Handles startup.
 * Shows start screen. Language can be changed as Unity takes device language.
 * For first app start onboarding is shown. Otherwise dashboard.
 * 
 */
public class StartupController : MonoBehaviour
{
    // UI
    public GameObject languageDialog;

    // tutorial
    public TutorialController tutorialController;

    // locales
    public Locale english;
    public Locale german;
    public Locale french;

    void Start()
    {
        SetLocaleFromSystemLanguage();
        ShowLanguageDialog(true);
    }

    void SetLocaleFromSystemLanguage()
    {
        Debug.Log("LANGUAGE SYSTEM: " + Application.systemLanguage.ToString());
        string systemLanguage = Application.systemLanguage.ToString();

        if (systemLanguage == "French")
        {
            LocalizationSettings.SelectedLocale = french;
        }
        else if (systemLanguage == "German")
        {
            LocalizationSettings.SelectedLocale = german;
        }
        else
        {
            LocalizationSettings.SelectedLocale = english;
        }
    }

    void HandleStartup()
    {

    }

    public void SetLanguage(Locale aLanguage)
    {
        LocalizationSettings.SelectedLocale = aLanguage;
    }

    public void ShowLanguageDialog(bool isShown)
    {
        languageDialog.GetComponent<CanvasGroup>().alpha = isShown ? 1 : 0;
        languageDialog.GetComponent<CanvasGroup>().blocksRaycasts = isShown;
        if (!isShown)
        {
            // we want to start tutorial, TODO remove later to do diffferent
            //tutorialController.StartTutorial();
        }
    }
}
