using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayListUtils : MonoBehaviour
{
    /**
     * Fills array with n null to have desired size.
     */
    public static ArrayList FillArrayList(int size)
    {
        ArrayList newList = new ArrayList();
        for (int i = 0; i < size; i++)
        {
            newList.Add(null);
        }
        return newList;
    }
}
