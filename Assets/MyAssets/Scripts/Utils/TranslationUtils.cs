﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

public class TranslationUtils : MonoBehaviour
{
    /**
     * Returns string for a key from Localization table UI Text.
     */
    public static string Translate(string key)
    {
        var op = LocalizationSettings.StringDatabase.GetLocalizedStringAsync("UI Text", key);
        string translation = "";
        if (op.IsDone)
            translation = op.Result;
        else
            op.Completed += (o) => translation = o.Result;

        return translation;
    }

    /**
     * Returns string for a key from Localization table Inventory.
     * This is ment for Inventory Items only.
     */
    public static string TranslateInventoryItem(string key)
    {
        var op = LocalizationSettings.StringDatabase.GetLocalizedStringAsync("InventoryItems", key);
        string translation = "";
        if (op.IsDone)
            translation = op.Result;
        else
            op.Completed += (o) => translation = o.Result;

        return translation;
    }
}
