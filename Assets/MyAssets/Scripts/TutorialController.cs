using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialController : MonoBehaviour
{
    private bool isActive = false;

    // quest giver
    public QuestGiver questGive;

    // UI
    public GameObject tutorialDialog;
    public TextMeshProUGUI dialogText;

    // index at which point of tutorial we are currently
    private int tutorialIndex = 0;

    // number of items picked up
    private int itemCount = 0;

    // objects to pick up during tutorial part 3, is done here instead of quest goal because we don't want to spawn right away
    public GameObject spawningParent;
    public List<Transform> spawnPoints;
    public List<Item> itemsToCollect;

    private void Awake()
    {
        //// deactivate game objects used for spawn points
        //foreach (Transform spawnPoint in spawnPoints)
        //{
        //    spawnPoint.gameObject.SetActive(false);
        //}
    }

    /**
     * Start the tutorial dialogs.
     */
    public void StartTutorial()
    {
        isActive = true;
        tutorialIndex = 0;
        itemCount = 0;
        ShowInfo1();
    }

    /**
     * Info: Welcome
     */
    private void ShowInfo1()
    {
        tutorialIndex = 1;
        dialogText.text = TranslationUtils.Translate("INTRO_WELCOME");
        ShowTutorialDialoge(true);
    }

    /**
     * Info: Left controller
     */
    private void ShowInfo2()
    {
        tutorialIndex = 2;
        dialogText.text = TranslationUtils.Translate("INTRO_MOVEMENT_1");
        ShowTutorialDialoge(true);
    }

    /**
     * Info: Right to control camera, collect items
     */
    private void ShowInfo3()
    {
        tutorialIndex = 3;
        dialogText.text = TranslationUtils.Translate("INTRO_MOVEMENT_2");
        ShowTutorialDialoge(true);
    }

    /**
     * Info: Open inventory and use item, show MB
     */
    private void ShowInfo4()
    {
        tutorialIndex = 4;
        dialogText.text = TranslationUtils.Translate("INTRO_BACKPACK");
        ShowTutorialDialoge(true);
    }

    /**
     * Handles next button off dialoges.
     */
    public void ClickedNext()
    {
        if (tutorialIndex == 1)
        {
            ShowInfo2();
        }
        else if (tutorialIndex == 2)
        {
            ShowInfo3();
        }
        else if (tutorialIndex == 3)
        {
            ShowTutorialDialoge(false);

            // user should collect items
            SpawnObjects();
        }
        else if (tutorialIndex == 4)
        {
            // user should open barrier by using MB
            ShowTutorialDialoge(false);
        }
    }

    /**
     * Spawn items to be collected by user.
     */
    private void SpawnObjects()
    {
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            ItemWorld.SpawnItemWorldToParent(spawnPoints[i].position, Quaternion.identity, itemsToCollect[i], spawningParent);

        }
    }

    /**
     * Called when item is collected.
     * Goes to next dialoge when all are collected
     */
    public void CollectedItem()
    {
        itemCount++;
        if (itemCount == 4)
        {
            ShowInfo4();
        }
    }

    /**
     * Util to show and hide tutorial dialoges.
     */
    public void ShowTutorialDialoge(bool isShown)
    {
        tutorialDialog.GetComponent<CanvasGroup>().alpha = isShown ? 1 : 0;
        tutorialDialog.GetComponent<CanvasGroup>().blocksRaycasts = isShown;
    }

    /**
     * True when tutorial is active.
     */
    public bool IsTutorialActive()
    {
        return isActive;
    }
}
