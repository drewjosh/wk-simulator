using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundtrackController : MonoBehaviour
{
    public List<AudioClip> soundtracks;
    private AudioSource audioSource;
    private int currentPlayingIndex = -1; // so we start at 0
    private bool isQuestSoundtrackPlaying = false;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!audioSource.isPlaying && !isQuestSoundtrackPlaying)
        {
            if (currentPlayingIndex + 1 < soundtracks.Count)
            {
                // play next song, if there is a next one
                currentPlayingIndex++;
            }
            else
            {
                currentPlayingIndex = 0;
            }
            audioSource.clip = soundtracks[currentPlayingIndex];
            audioSource.Play();
        }
    }

    public void StopCurrentSoundtrack()
    {
        isQuestSoundtrackPlaying = false;
        audioSource.Stop();
    }

    public void PlaySoundtrack(AudioClip newSoundtrack)
    {
        isQuestSoundtrackPlaying = true;
        audioSource.Stop();
        audioSource.clip = newSoundtrack;
        audioSource.Play();
    }
}
