using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Inventory inventory;
    [SerializeField] private UI_Inventory inventoryUI;

    public int health = 5;
    public int money = 150;
    public int experience = 0;

    public Quest currentQuest;

    private TutorialController tutorialController;

    private void Awake()
    {
        tutorialController = GameObject.Find("TutorialController").GetComponent<TutorialController>();
    }

    void Start()
    {
        inventory = new Inventory();
        inventoryUI.SetInventory(inventory);
        inventoryUI.SetPlayer(this);
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.beret));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.dogtag));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.jacketTenueB));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.trouserTenueB));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.trouserLaces));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.socks));
        inventory.AddItemToInventoryExtension(Item.ItemType.rolli, new Item(Item.ItemType.ks90));
    }

    private void OnTriggerEnter(Collider other)
    {
        ItemWorld itemWorld = other.GetComponent<ItemWorld>();
        if (itemWorld != null)
        {
            // Touching item and item can be picked up into inventory
            if (inventory.AddItem(itemWorld.GetItem()))
            {
                itemWorld.DestroySelf();
            }
            else
            {
                Debug.Log("No space in inventory anymore, please use a combat bag or something to carry!");
            }

            //if (currentQuest != null && currentQuest.isActive && !tutorialController.IsTutorialActive())
            //{
            //    // checking for picked up items of inventory
            //    currentQuest.goal.ItemCollected(itemWorld.GetItem());
            //    if (currentQuest.goal.IsReached())
            //    {
            //        // quest was reached!
            //        Debug.Log("Quest was finished congrats!");
            //        currentQuest.Complete();
            //        experience += currentQuest.experienceReward;
            //        money += currentQuest.moneyReward;
            //        currentQuest = null;
            //    }
            //}
            //else if (currentQuest != null && currentQuest.isActive && tutorialController.IsTutorialActive())
            //{
            //    // collected during tutorial
            //    tutorialController.CollectedItem();
            //}
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Transform GetTransform()
    {
        return transform;
    }
}
