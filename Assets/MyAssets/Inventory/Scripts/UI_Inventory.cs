using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UI_Inventory : MonoBehaviour
{
    private Inventory inventory;
    private Player player;

    // clothing equipment
    public GameObject clothingSlotsParent;
    public GameObject clothingItemTemplate;

    // inventory equipment
    public Transform itemsAndLuggageContainer;
    private Transform equipmentItemSlotContainer;
    private Transform equipmentItemSlotTemplate;
    private Transform equipmentItemsContainer;
    private Transform equipmentItemTemplate;
    public TextMeshProUGUI currentEquipmentSelectionTitle;
    public List<Button> equipmentCategoryButtons;
    private int currentEquipmentSelection = 0;
    public GameObject bodyItemsView;

    // inventory extension
    public Transform extensionSlotsContainer;
    public Transform extensionItemsContainer;
    private Transform extensionSlotTemplate;
    private Transform extensionItemTemplate;
    public TextMeshProUGUI currentExtensionSelectionTitle;
    public List<Button> extensionCategoryButtons;
    private int currentExtensionSelection = 0;

    // currently selected item
    private Item currentActiveItem; // to know which item is currently selected
    public GameObject currentSelectionView; // here we show info about selected item
    private Image currentSelectionImage;
    private TextMeshProUGUI currentItemSelectionTitle;
    private TextMeshProUGUI currentItemSelectionDescription;

    // need reference to activate/deactivate when inventory is open to not move player around
    private ThirdPersonOrbitCamBasicMobile thirdPersonCam;

    private void Awake()
    {
        // inventory extensions
        extensionSlotTemplate = extensionSlotsContainer.transform.Find("SlotTemplate");
        extensionItemTemplate = extensionItemsContainer.transform.Find("ItemTemplate");

        // items inventory slots
        equipmentItemSlotContainer = itemsAndLuggageContainer.transform.Find("SlotContainer");
        equipmentItemSlotTemplate = equipmentItemSlotContainer.transform.Find("SlotTemplate");
        equipmentItemsContainer = itemsAndLuggageContainer.transform.Find("ItemsContainer");
        equipmentItemTemplate = equipmentItemsContainer.transform.Find("ItemTemplate");

        // current item
        currentSelectionImage = currentSelectionView.transform.Find("image").GetComponent<Image>();
        currentItemSelectionTitle = currentSelectionView.transform.Find("title").GetComponent<TextMeshProUGUI>();
        currentItemSelectionDescription = currentSelectionView.transform.Find("description").GetComponent<TextMeshProUGUI>();

        thirdPersonCam = GameObject.Find("Main Camera").GetComponent<ThirdPersonOrbitCamBasicMobile>();
    }

    /**
     * Sets up inventory
     */
    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;

        // subscribe to item list changed event
        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventory();
    }

    /**
     * Sets player.
     * Access to players position needed for item drop,
     */
    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    /**
     * Toggles visibility of inventory.
     * Toggles also bbasic mobile movement (looking around) of player to not move it.
     */
    public void ToggleInventoryOnClick()
    {
        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = canvasGroup.alpha == 0 ? 1 : 0;
        canvasGroup.blocksRaycasts = canvasGroup.alpha == 1 ? true : false;

        // activate/deactivate when inventory is open to not move player around
        thirdPersonCam.enabled = canvasGroup.alpha == 1 ? false : true;

        if (GetComponent<CanvasGroup>().alpha == 0)
        {
            // inventory is hidden -> reset current item and selected slot
            // TODO
        }
        else
        {
            RefreshInventory();
            RefreshCurrentViewTitles();
        }
    }

    /**
     * Event when item list changed
     */
    private void Inventory_OnItemListChanged(object sender, EventArgs e)
    {
        RefreshInventory();
    }

    /**
     * Refreshes drawing the inventory.
     */
    private void RefreshInventory()
    {
        RefreshInventoryEquipment();
        RefreshInventoryExtension();
    }

    /**
     * Refresh titles of the currenlty selected categories;
     */
    private void RefreshCurrentViewTitles()
    {
        // equipment section
        if (currentEquipmentSelection == 0)
        {
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("CLOTHING");
        }
        else if (currentEquipmentSelection == 1)
        {
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("ITEMS");
        }
        else if (currentEquipmentSelection == 2)
        {
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("LUGGAGE");
        }
        else if (currentEquipmentSelection == 3)
        {
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("EXTERNAL_STORAGE");
        }

        // inventory extension section
        Item.ItemType extensionType = GetInventoryExtensionTypeByButtonIndex(currentExtensionSelection);
        InventoryExtension inventoryExtension = inventory.GetInventoryExtensionByItemType(extensionType);
        currentExtensionSelectionTitle.text = inventoryExtension.GetTitle();
    }

    /*
     * Draw inventory equipment according to current selection.
     */
    private void RefreshInventoryEquipment()
    {
        // deactivate body items as default, so only have to enable in one place
        bodyItemsView.GetComponent<CanvasGroup>().alpha = 0;
        bodyItemsView.GetComponent<CanvasGroup>().blocksRaycasts = false;

        ResetEquipmentItemsList();

        if (currentEquipmentSelection == 0)
        {
            // body items
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("CLOTHING");
            bodyItemsView.GetComponent<CanvasGroup>().alpha = 1;
            bodyItemsView.GetComponent<CanvasGroup>().blocksRaycasts = true;

            RedrawEquipmentClothing();
        }
        else if (currentEquipmentSelection == 1)
        {
            // items
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("ITEMS");
            DrawEquipmentItemList(inventory.equipmentItemList, Item.ItemSlotType.item);
        }
        else if (currentEquipmentSelection == 2)
        {
            // luggage
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("LUGGAGE");
            DrawEquipmentItemList(inventory.luggageItems, Item.ItemSlotType.luggage);
        }
        else if (currentEquipmentSelection == 3)
        {
            // external items
            currentEquipmentSelectionTitle.text = TranslationUtils.Translate("EXTERNAL_STORAGE");
            // TODO render slots from external item list
        }
    }

    /**
     * Removes all items from equipment items list to render new.
     */
    private void ResetEquipmentItemsList()
    {
        // remove old transforms for all other views except body items before drawing new, except template
        foreach (Transform child in equipmentItemsContainer)
        {
            if (child == equipmentItemTemplate) continue;
            Destroy(child.gameObject);
        }
        foreach (Transform child in equipmentItemSlotContainer)
        {
            if (child == equipmentItemSlotTemplate) continue;
            Destroy(child.gameObject);
        }
    }

    /**
     * Draw list for equipment categories: items, luggage and external storage
     */
    private void DrawEquipmentItemList(ArrayList itemList, Item.ItemSlotType slotType)
    {
        int x = 0;
        int y = 0;
        float storageSlotCellSize = 210f;
        for (int i = 0; i < itemList.Count; i++)
        {
            // loop through items of this equipment list

            // we always draw a background to have a slot
            RectTransform slotRectTransform = Instantiate(equipmentItemSlotTemplate, equipmentItemSlotContainer).GetComponent<RectTransform>();
            slotRectTransform.gameObject.SetActive(true);
            ItemSlot itemSlot = slotRectTransform.transform.GetComponent<ItemSlot>();
            itemSlot.slotIndex = i;
            itemSlot.parentList = itemList;
            itemSlot.slotType = slotType;

            // set or disable item image
            RectTransform itemRectTransform = Instantiate(equipmentItemTemplate, equipmentItemsContainer).GetComponent<RectTransform>();
            Image image = itemRectTransform.GetComponent<Image>();
            if (itemList[i] != null)
            {
                Item item = (Item)itemList[i];
                image.enabled = true;
                image.sprite = item.sprite;
                item.currentSlotIndex = i;
                item.parentList = itemList;
                itemRectTransform.gameObject.SetActive(true);
                itemRectTransform.transform.GetComponent<DragDrop>().item = item;
                itemRectTransform.transform.GetComponent<DragDrop>().SetInventory(inventory);
            }
            else
            {
                // disabble image so we have free slot
                image.enabled = false;
                itemRectTransform.gameObject.SetActive(false);
            }

            // show items in a grid
            slotRectTransform.anchoredPosition = new Vector2(x * storageSlotCellSize, y * storageSlotCellSize * -1);
            itemRectTransform.anchoredPosition = new Vector2(x * storageSlotCellSize, y * storageSlotCellSize * -1);
            x++;
            if (x > 3)
            {
                // grid width = 4
                x = 0;
                y++;
            }
        }
    }

    private int GetExtensionButtonIndexByType(Item.ItemType type)
    {
        switch (type)
        {
            default:
                return -1;
            case Item.ItemType.karusa:
                return 0;
            case Item.ItemType.gte:
                return 1;
            case Item.ItemType.tarusa:
                return 2;
            case Item.ItemType.ftasche:
                return 3;
            case Item.ItemType.rolli:
                return 4;
        }
    }

    /**
     * Return inventory extension type by button index.
     */
    private Item.ItemType GetInventoryExtensionTypeByButtonIndex(int index)
    {
        switch (index)
        {
            default:
                return Item.ItemType.chocholate;
            case 0:
                return Item.ItemType.karusa;
            case 1:
                return Item.ItemType.gte;
            case 2:
                return Item.ItemType.tarusa;
            case 3:
                return Item.ItemType.ftasche;
            case 4:
                return Item.ItemType.rolli;
        }
    }

    /**
     * Draw inventory extensions according to current selection.
     */
    private void RefreshInventoryExtension()
    {
        // remove old transforms before drawing new, except template
        foreach (Transform child in extensionItemsContainer)
        {
            if (child == extensionItemTemplate) continue;
            Destroy(child.gameObject);
        }
        foreach (Transform child in extensionSlotsContainer)
        {
            if (child == extensionSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        // activate/deactivate buttons according to availability
        int lowestIndexOfAvailableExtension = 10;
        ArrayList availableExtensionsByIndex = new ArrayList();
        foreach (Item extensionItem in inventory.luggageItems)
        {
            if (extensionItem != null)
            {
                int buttonIndex = GetExtensionButtonIndexByType(extensionItem.itemType);
                extensionCategoryButtons[buttonIndex].interactable = true;
                availableExtensionsByIndex.Add(buttonIndex);

                if (currentExtensionSelection == -1 && buttonIndex < lowestIndexOfAvailableExtension)
                {
                    // save lowest index to select it if there was no selection before
                    lowestIndexOfAvailableExtension = buttonIndex;
                    currentExtensionSelection = buttonIndex;
                }
            }
        }

        // deactivate buttons that are not available
        for (int i = 0; i < extensionCategoryButtons.Count; i++)
        {
            if (!availableExtensionsByIndex.Contains(i))
            {
                extensionCategoryButtons[i].interactable = false;
            }
        }

        if (currentExtensionSelection == -1)
        {
            // no extension available, we clear the text
            currentExtensionSelectionTitle.text = "";
        }
        else
        {
            //extensionCategoryButtons[currentExtensionSelection].gameObject.GetComponent<Image>().color = new Color32(32, 255, 225, 100);
            ////Color green = new Color(39, 185, 64);
            //extensionCategoryButtons[currentExtensionSelection].gameObject.GetComponent<Image>().color = green;

            // draw item list of currently selected extension
            Item.ItemType extensionType = GetInventoryExtensionTypeByButtonIndex(currentExtensionSelection);
            InventoryExtension inventoryExtension = inventory.GetInventoryExtensionByItemType(extensionType);
            currentExtensionSelectionTitle.text = inventoryExtension.GetTitle();
            int x = 0;
            int y = 0;
            float storageSlotCellSize = 210f;

            ArrayList itemList = inventoryExtension.GetItems();
            for (int i = 0; i < itemList.Count; i++)
            {
                // loop through items of this extension

                // draw the slot
                RectTransform itemSlotRectTransform = Instantiate(extensionSlotTemplate, extensionSlotsContainer).GetComponent<RectTransform>();
                itemSlotRectTransform.gameObject.SetActive(true);
                ItemSlot itemSlot = itemSlotRectTransform.transform.GetComponent<ItemSlot>();
                itemSlot.slotIndex = i;
                itemSlot.parentList = itemList;
                itemSlot.slotType = Item.ItemSlotType.luggageExtension;
                itemSlot.parentItemType = extensionType; // save item type of parent of where the item is currently placed inside

                // draw the item
                RectTransform itemRectTransform = Instantiate(extensionItemTemplate, extensionItemsContainer).GetComponent<RectTransform>();

                // set or disable item image
                Image image = itemRectTransform.GetComponent<Image>();
                if (itemList[i] != null)
                {
                    // there is an item
                    Item item = (Item)itemList[i];
                    image.enabled = true;
                    image.sprite = item.sprite;
                    item.currentSlotIndex = i;
                    item.parentList = itemList;
                    itemRectTransform.gameObject.SetActive(true);
                    itemRectTransform.transform.GetComponent<DragDrop>().item = item;
                    itemRectTransform.transform.GetComponent<DragDrop>().SetInventory(inventory);
                }
                else
                {
                    // disabble image so we have free slot
                    image.enabled = false;
                    itemRectTransform.gameObject.SetActive(false);
                }

                // show slots and items in a grid
                itemSlotRectTransform.anchoredPosition = new Vector2(x * storageSlotCellSize, y * storageSlotCellSize * -1);
                itemRectTransform.anchoredPosition = new Vector2(x * storageSlotCellSize, y * storageSlotCellSize * -1);
                x++;
                if (x > 4)
                {
                    // grid width = 5
                    x = 0;
                    y++;
                }
            }
        }
    }

    /**
     * Handles click on inventory equipment button.
     */
    public void SelectInventoryEquipmentClick(int index)
    {
        if (index != currentEquipmentSelection)
        {
            // other equipment was selected then already showing
            currentEquipmentSelection = index;
            RefreshInventoryEquipment();
            ResetActiveItem();
        }
    }

    /**
     * Handles click on inventory extension button.
     */
    public void SelectInventoryExtensionClick(int index)
    {
        if (index != currentExtensionSelection)
        {
            // other extension was selected then already showing
            currentExtensionSelection = index;
            RefreshInventoryExtension();
            ResetActiveItem();
        }
    }

    /**
     * Handle click on an item.
     */
    public void ActiveItemOnClick(Item item)
    {
        if (item != null)
        {
            currentSelectionView.GetComponent<CanvasGroup>().alpha = 1;
            currentActiveItem = item;
            currentItemSelectionTitle.text = item.GetTitle();
            currentItemSelectionDescription.text = item.GetDescription();
            currentSelectionImage.enabled = true;
            currentSelectionImage.sprite = item.sprite;
        }
        else
        {
            ResetActiveItem();
        }
    }

    /**
     * Hides view of currently selected item.
     */
    private void ResetActiveItem()
    {
        currentActiveItem = null;
        currentSelectionView.GetComponent<CanvasGroup>().alpha = 0;
    }

    /**
     * Handle item drop.
     * Item is droped in front of player as 3d model.
     */
    public void DropItemOnClick()
    {
        //Item duplicateItem = new Item { itemType = currentActiveItem.itemType, amount = currentActiveItem.amount };
        //ItemWorld.DropItem(player.GetTransform(), duplicateItem);
        //inventory.RemoveItem(currentActiveItem);
        //ResetActiveItem();
    }

    /**
     * Draw clothing according to clothing list.
     */
    private void RedrawEquipmentClothing()
    {
        for (int i = 0; i < clothingSlotsParent.transform.childCount; i++)
        {
            ItemSlot slot = clothingSlotsParent.transform.GetChild(i).GetComponentInChildren<ItemSlot>();

            // remove old first
            RectTransform itemParent = clothingSlotsParent.transform.GetChild(i).Find("itemParent").GetComponent<RectTransform>();
            if (itemParent.childCount > 0 && itemParent.GetChild(0) != null)
            {
                Destroy(itemParent.GetChild(0).gameObject);
            }

            for (int j = 0; j < inventory.clothingItems.Count; j++)
            {
                if (inventory.clothingItems[j] != null && inventory.clothingItems[j].itemSlotType == slot.slotType)
                {
                    RectTransform itemRectTransform = Instantiate(equipmentItemTemplate, itemParent).GetComponent<RectTransform>();
                    itemRectTransform.gameObject.SetActive(true);
                    DragDrop itemDragDrop = itemRectTransform.GetComponent<DragDrop>();
                    itemDragDrop.SetInventory(inventory);
                    Image itemImage = itemDragDrop.gameObject.transform.GetComponent<Image>();
                    itemDragDrop.item = inventory.clothingItems[j];
                    itemImage.sprite = inventory.clothingItems[j].sprite;
                    itemImage.enabled = true;
                    break;
                }
            }
        }



    }
}
