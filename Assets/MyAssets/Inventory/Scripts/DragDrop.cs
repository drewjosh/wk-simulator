using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private Canvas canvas;

    private RectTransform rectTransform;
    private Vector2 rectStartPosition; // to perform reset when 1) cannot drag, 2) not dragged on item slot
    private CanvasGroup canvasGroup;
    public Item item;

    public bool dropSuccess = false;

    private UI_Inventory uI_Inventory; // access to handle on item click for info
    private Inventory inventory; // access inventory functions to alter dragged items

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        uI_Inventory = GameObject.Find("UI_Inventory").GetComponent<UI_Inventory>(); ;
    }

    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        rectStartPosition = new Vector2(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y);
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = .6f;
        dropSuccess = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;

        if (!dropSuccess)
        {
            // return item where we started drag
            rectTransform.anchoredPosition = rectStartPosition;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        uI_Inventory.ActiveItemOnClick(item);
    }

    /**
     * Handle item was dropped successfully.
     * Notify inventory about change.
     */
    public void HandleDropped(int newSlotIndex, ArrayList newParentList)
    {
        dropSuccess = true;
        inventory.HandleNewItemOrder(item, newSlotIndex, newParentList);
    }
}