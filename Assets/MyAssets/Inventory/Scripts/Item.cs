using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Represents item that user can pick up in inventory.
 */
[Serializable]
public class Item
{
    // properties to define item
    public string titleKey;
    public string descriptionKey;
    public ItemType itemType;
    public Sprite sprite;
    public Transform prefab;

    // properties to handle inventory
    public ItemSlotType itemSlotType; // needed for inventory
    public int currentSlotIndex; // in which position is this item in list
    public ArrayList parentList; // to which list belongs this item

    // color of item if can have different one, need to be set from extern
    public ItemColor color;

    /**
     * Automatic setup in constructor according to item type.
     */
    public Item(ItemType type)
    {
        this.itemType = type;
        ItemSetup();
    }

    // all unique items
    public enum ItemType
    {
        marschbefehl,
        beret,
        mutz,
        hat,
        dogtag,
        shirtOld,
        shirtNew,
        jacketTenueA,
        jacketTenueB,
        jacketTenueC,
        jacketRain,
        jacketABC,
        gortex,
        pants,
        socks,
        trouserTenueA,
        trouserTenueB,
        trouserTenueC,
        trouserRain,
        trouserABC,
        trouserLaces,
        belt,
        ks90,
        ks14,
        ks15,
        shoesABC,
        glovesCombat,
        glovesSnow,
        ammunitionStgwr90,
        ammunitionPistol,
        sleepingBag,
        sleepingMat,
        karusa,
        gte,
        tarusa,
        ftasche,
        rolli,
        ivp,
        gasmask,
        gasmaskUtilities,
        chocholate,
        notfallzettel,
        id,
        earplug,
        badgeWKSimulator,
        stgwr90,
        pistol,
        stgwr90Ammo,
        pistolAmmo,
        stgwr90PDMat,
        pistolPDMat,
        emergencyCooker,
        chillyJonny,
        hg,
        pocketknife,
        drinkingBottle,
        tourniquet,
        armbandMedic,
        pamir,
        tacticalGlasses,
        flashlight,
        matches
    }

    // available colors for certain items
    public enum ItemColor
    {
        blue,
        green,
        black,
        gray
    }

    // all possible slot for items
    public enum ItemSlotType
    {
        headgear,
        glasses,
        neck,
        shirt,
        jacket,
        badge,
        underwear,
        socks,
        trouser,
        trouserLaces,
        belt,
        shoes,
        gloves,
        ammunition,
        luggage,
        luggageExtension,
        item,
    }

    /**
     * Returns true if item is a clothing that can bbe eqipped to character.
     */
    public bool IsClothing()
    {
        if (itemSlotType == ItemSlotType.ammunition || itemSlotType == ItemSlotType.luggage ||
            itemSlotType == ItemSlotType.luggageExtension || itemSlotType == ItemSlotType.item)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Sets up item with all its properties.
     */
    private void ItemSetup()
    {
        switch (itemType)
        {
            default:

            // normal items
            case ItemType.ivp:
                titleKey = "IVP";
                descriptionKey = "IVP_DETAIL";
                prefab = ItemAssets.Instance.ivpPrefab;
                sprite = ItemAssets.Instance.ivpSprite;
                itemSlotType = ItemSlotType.item;
                break;
            case ItemType.notfallzettel:
                titleKey = "NOTFALLZETTEL";
                descriptionKey = "NOTFALLZETTEL_DETAIL";
                prefab = ItemAssets.Instance.notfallzettelPrefab;
                sprite = ItemAssets.Instance.notfallzettelSprite;
                itemSlotType = ItemSlotType.item;
                break;
            case ItemType.marschbefehl:
                titleKey = "MB";
                descriptionKey = "MB_DETAIL";
                prefab = ItemAssets.Instance.marschbefehlPrefab;
                sprite = ItemAssets.Instance.marschbefehlSprite;
                itemSlotType = ItemSlotType.item;
                break;

            // clothing items
            case ItemType.beret:
                titleKey = "BERET";
                descriptionKey = "BERET_DETAIL";
                prefab = ItemAssets.Instance.beretPrefab;
                sprite = ItemAssets.Instance.beretSprite;
                itemSlotType = ItemSlotType.headgear;
                break;
            case ItemType.shirtOld:
                titleKey = "SHIRT_OLD";
                descriptionKey = "SHIRT_OLD_DETAIL";
                prefab = ItemAssets.Instance.shirtOldProfab;
                sprite = ItemAssets.Instance.shirtOldSprite;
                itemSlotType = ItemSlotType.shirt;
                break;
            case ItemType.shirtNew:
                titleKey = "SHIRT_NEW";
                descriptionKey = "SHIRT_NEW_DETAIL";
                prefab = ItemAssets.Instance.shirtNewPrefab;
                sprite = ItemAssets.Instance.shirtNewSprite;
                itemSlotType = ItemSlotType.shirt;
                break;
            case ItemType.jacketTenueB:
                titleKey = "JACKET_B";
                descriptionKey = "JACKET_B_DETAIL";
                prefab = ItemAssets.Instance.jacketTenueBPrefab;
                sprite = ItemAssets.Instance.jacketTenueBSprite;
                itemSlotType = ItemSlotType.jacket;
                break;
            case ItemType.pants:
                titleKey = "BOXERSHORTS";
                descriptionKey = "BOXERSHORTS_DETAIL";
                prefab = ItemAssets.Instance.underwearCPrefab;
                sprite = ItemAssets.Instance.underwearSprite;
                itemSlotType = ItemSlotType.underwear;
                break;
            case ItemType.belt:
                titleKey = "BELT";
                descriptionKey = "BELT_DETAIL";
                prefab = ItemAssets.Instance.beltGreenPrefab;
                sprite = ItemAssets.Instance.beltGreenSprite;
                itemSlotType = ItemSlotType.underwear;
                break;
            case ItemType.trouserLaces:
                titleKey = "TROUSER_LACES";
                descriptionKey = "TROUSER_LACES_DETAIL";
                prefab = ItemAssets.Instance.trouserLacesPrefab;
                sprite = ItemAssets.Instance.trouserLacesSprite;
                itemSlotType = ItemSlotType.trouserLaces;
                break;
            case ItemType.trouserTenueB:
                titleKey = "TROUSER_B";
                descriptionKey = "TROUSER_B_DETAIL";
                prefab = ItemAssets.Instance.trouserTenueBPrefab;
                sprite = ItemAssets.Instance.trouserTenueBSprite;
                itemSlotType = ItemSlotType.trouser;
                break;
            case ItemType.socks:
                titleKey = "SOCKS";
                descriptionKey = "SOCKS_DETAIL";
                prefab = ItemAssets.Instance.socksGrayPrefab;
                sprite = ItemAssets.Instance.socksGraySprite;
                itemSlotType = ItemSlotType.socks;
                break;
            case ItemType.ks90:
                titleKey = "KS90";
                descriptionKey = "KS90_DETAIL";
                prefab = ItemAssets.Instance.ks90Preffab;
                sprite = ItemAssets.Instance.ks90Sprite;
                itemSlotType = ItemSlotType.shoes;
                break;
            case ItemType.dogtag:
                titleKey = "DOGTAG";
                descriptionKey = "DOGTAG_DETAIL";
                prefab = ItemAssets.Instance.dogtagPrefab;
                sprite = ItemAssets.Instance.dogtagSprite;
                itemSlotType = ItemSlotType.neck;
                break;


            // inventory extension items
            case ItemType.karusa:
                titleKey = "KARUSA";
                descriptionKey = "KARUSA_DETAIL";
                prefab = ItemAssets.Instance.karusaPrefab;
                sprite = ItemAssets.Instance.karusaSprite;
                itemSlotType = ItemSlotType.luggage;
                break;
            case ItemType.gte:
                titleKey = "GTE";
                descriptionKey = "GTE_DETAIL";
                prefab = ItemAssets.Instance.gtePrefab;
                sprite = ItemAssets.Instance.gteSprite;
                itemSlotType = ItemSlotType.luggage;
                break;
            case ItemType.tarusa:
                titleKey = "TARUSA";
                descriptionKey = "TARUSA_DETAIL";
                prefab = ItemAssets.Instance.tarusaPrefab;
                sprite = ItemAssets.Instance.tarusaSprite;
                itemSlotType = ItemSlotType.luggage;
                break;
            case ItemType.ftasche:
                titleKey = "FTASCHE";
                descriptionKey = "FTASCHE_DETAIL";
                prefab = ItemAssets.Instance.ftaschePrefab;
                sprite = ItemAssets.Instance.ftascheSprite;
                itemSlotType = ItemSlotType.luggage;
                break;
            case ItemType.rolli:
                titleKey = "ROLLI";
                descriptionKey = "ROLLI_DETAIL";
                prefab = ItemAssets.Instance.rolliPrefab;
                sprite = ItemAssets.Instance.rolliSprite;
                itemSlotType = ItemSlotType.luggage;
                break;
        }
    }

    /**
     * Returns wether item fits into jacket or trouser pockets.
     */
    public bool IsFittingPocket()
    {
        switch (itemType)
        {
            default:
            case ItemType.marschbefehl:
            case ItemType.beret:
            case ItemType.mutz:
            case ItemType.hat:
            case ItemType.dogtag:
            case ItemType.pants:
            case ItemType.socks:
            case ItemType.trouserLaces:
            case ItemType.belt:
            case ItemType.glovesCombat:
            case ItemType.glovesSnow:
            case ItemType.ammunitionStgwr90:
            case ItemType.ammunitionPistol:
            case ItemType.ivp:
            case ItemType.chocholate:
            case ItemType.notfallzettel:
            case ItemType.id:
            case ItemType.earplug:
            case ItemType.badgeWKSimulator:
            case ItemType.stgwr90Ammo:
            case ItemType.pistolAmmo:
            case ItemType.pamir:
            case ItemType.tacticalGlasses:
            case ItemType.flashlight:
            case ItemType.matches:
                return true;
            case ItemType.drinkingBottle:
            case ItemType.gasmask:
            case ItemType.gasmaskUtilities:
            case ItemType.karusa:
            case ItemType.tarusa:
            case ItemType.gte:
            case ItemType.ftasche:
            case ItemType.rolli:
            case ItemType.jacketTenueA:
            case ItemType.jacketTenueB:
            case ItemType.jacketTenueC:
            case ItemType.jacketRain:
            case ItemType.jacketABC:
            case ItemType.gortex:
            case ItemType.trouserTenueA:
            case ItemType.trouserTenueB:
            case ItemType.trouserTenueC:
            case ItemType.trouserRain:
            case ItemType.trouserABC:
            case ItemType.ks90:
            case ItemType.ks14:
            case ItemType.ks15:
            case ItemType.shoesABC:
            case ItemType.sleepingBag:
            case ItemType.sleepingMat:
            case ItemType.stgwr90:
            case ItemType.pistol:
            case ItemType.stgwr90PDMat:
            case ItemType.pistolPDMat:
            case ItemType.emergencyCooker:
            case ItemType.chillyJonny:
            case ItemType.hg:
            case ItemType.pocketknife:
            case ItemType.tourniquet:
            case ItemType.armbandMedic:
                return false;
        }
    }

    /**
     * Returns translation of title.
     */
    public string GetTitle()
    {
        return TranslationUtils.TranslateInventoryItem(titleKey);
    }

    /**
     * Returns translation of description.
     */
    public string GetDescription()
    {
        return TranslationUtils.TranslateInventoryItem(descriptionKey);
    }
}
