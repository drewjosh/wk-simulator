using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Represents a luggage item like KaRuSa or Rolli that can also hold items.
 */
public class InventoryExtension
{
    public Item.ItemType type;
    public bool isInInventory;
    private ArrayList itemList;

    public InventoryExtension(Item.ItemType type)
    {
        this.type = type;
        isInInventory = true; // TODO in future we have to load this from storage
        itemList = new ArrayList(GetSize());
        itemList = ArrayListUtils.FillArrayList(GetSize()); // TODO in future we have to load from storage
    }

    /**
     * Adds item to first free slot and return true when success.
     */
    public bool AddItemToFirstPossible(Item item)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i] == null)
            {
                // add item to first free slot
                itemList[i] = item;
                return true;
            }
        }
        return false;
    }

    /**
     * Adds item to specific slot and returns true if success.
     */
    public bool AddItemByIndex(Item item, int index)
    {
        if (index < itemList.Count && itemList[index] == null)
        {
            itemList[index] = item;
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Sets flag if InventoryExtension was picked up by user.
     */
    public void SetIsInInventory(bool flag)
    {
        isInInventory = flag;
    }

    /**
     * Returns translated full name of the inventory extension.
     */
    public string GetTitle()
    {
        switch (type)
        {
            default:
                return "";
            case Item.ItemType.karusa:
                return TranslationUtils.TranslateInventoryItem("KARUSA");
            case Item.ItemType.gte:
                return TranslationUtils.TranslateInventoryItem("GTE");
            case Item.ItemType.tarusa:
                return TranslationUtils.TranslateInventoryItem("TARUSA");
            case Item.ItemType.ftasche:
                return TranslationUtils.TranslateInventoryItem("FTASCHE");
            case Item.ItemType.rolli:
                return TranslationUtils.TranslateInventoryItem("ROLLI");
        }
    }

    /**
     * Returns size of an inventory extension;
     */
    public int GetSize()
    {
        switch (type)
        {
            default:
                return 0;
            case Item.ItemType.karusa:
                return 8;
            case Item.ItemType.gte:
                return 5;
            case Item.ItemType.tarusa:
                return 6;
            case Item.ItemType.ftasche:
                return 12;
            case Item.ItemType.rolli:
                return 20;
        }
    }

    /**
     * Returns items of the extension.
     */
    public ArrayList GetItems()
    {
        return itemList;
    }
}
