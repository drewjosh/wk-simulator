using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAssets : MonoBehaviour
{
    public static ItemAssets Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    /** SPRITES **/

    //luggage
    public Sprite karusaSprite;
    public Sprite gteSprite;
    public Sprite tarusaSprite;
    public Sprite ftascheSprite;
    public Sprite rolliSprite;

    // clothing
    public Sprite beretSprite;
    public Sprite shirtNewSprite;
    public Sprite shirtOldSprite;
    public Sprite jacketTenueBSprite;
    public Sprite jacketTenueASPrite;
    public Sprite jacketTenueCSprite;
    public Sprite underwearSprite;
    public Sprite beltGreenSprite;
    public Sprite trouserLacesSprite;
    public Sprite socksGraySprite;
    public Sprite ks90Sprite;
    public Sprite dogtagSprite;
    public Sprite trouserTenueBSprite;

    // items
    public Sprite ivpSprite;
    public Sprite marschbefehlSprite;
    public Sprite notfallzettelSprite;


    /** PREFABBS **/

    //luggage
    public Transform karusaPrefab;
    public Transform gtePrefab;
    public Transform tarusaPrefab;
    public Transform ftaschePrefab;
    public Transform rolliPrefab;

    // clothing
    public Transform beretPrefab;
    public Transform shirtNewPrefab;
    public Transform shirtOldProfab;
    public Transform jacketTenueBPrefab;
    public Transform jacketTenueAPrefab;
    public Transform jacketTenueCPrefab;
    public Transform underwearCPrefab;
    public Transform beltGreenPrefab;
    public Transform trouserLacesPrefab;
    public Transform socksGrayPrefab;
    public Transform ks90Preffab;
    public Transform dogtagPrefab;
    public Transform trouserTenueBPrefab;


    // items
    public Transform ivpPrefab;
    public Transform marschbefehlPrefab;
    public Transform notfallzettelPrefab;
}
