using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Holds all parts of player character to enabble/disable in inventory.
 */
public class CharacterAssets : MonoBehaviour
{
    /**
     * Clothing items of player
     */
    public GameObject trouser_tenue_b;
}
