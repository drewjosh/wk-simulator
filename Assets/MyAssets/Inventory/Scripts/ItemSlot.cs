using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IDropHandler
{
    public Item.ItemSlotType slotType;
    public int slotIndex;
    public ArrayList parentList;

    // save item type of item representing this container, for example Karusa
    // used to prevent dropping karusa inside karusa, or Rolli inside F-Tasche 
    public Item.ItemType parentItemType;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
        if (eventData.pointerDrag != null)
        {
            // check that destination drag slot type is correct, to ItemSlotType.item or .luggageExtension we can drop everything
            Item.ItemSlotType draggedItemSlotType = eventData.pointerDrag.GetComponent<DragDrop>().item.itemSlotType;
            if (draggedItemSlotType == slotType || slotType == Item.ItemSlotType.item || slotType == Item.ItemSlotType.luggageExtension)
            {
                Item draggedItem = eventData.pointerDrag.GetComponent<DragDrop>().item;
                // check that user cannot put inventory extensions inside other impossible luggage
                Item.ItemType draggedItemType = draggedItem.itemType;
                if (draggedItemSlotType == Item.ItemSlotType.luggage && slotType == Item.ItemSlotType.luggageExtension)
                {
                    // trying to move luggage inside luggage

                    if (!(draggedItemType == Item.ItemType.gte && parentItemType == Item.ItemType.rolli))
                    {
                        // only gte can go inside rolli
                        return;
                    }
                }

                if (draggedItem.IsClothing() && slotIndex == -1)
                {
                    // we moved item to clothing list
                    eventData.pointerDrag.GetComponent<DragDrop>().HandleDropped(slotIndex, null);
                }
                else
                {
                    // we moved item away from clothing list, or all other cases
                    eventData.pointerDrag.GetComponent<DragDrop>().HandleDropped(slotIndex, parentList);

                    // set new parent first, to get position right 
                    eventData.pointerDrag.GetComponent<RectTransform>().SetParent(GetComponent<RectTransform>().parent.parent.Find("ItemsContainer"));
                    eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
                }
            }
        }
    }
}
