using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemWorld : MonoBehaviour
{
    private Item item;

    public static ItemWorld SpawnItemWorld(Vector3 position, Quaternion rotation, Item item)
    {
        Transform transform = Instantiate(item.prefab, position, rotation);
        ItemWorld itemWorld = transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        return itemWorld;
    }

    public static ItemWorld SpawnItemWorldToParent(Vector3 position, Quaternion rotation, Item item, GameObject parent)
    {
        Transform transformItem = Instantiate(item.prefab, position, rotation);
        transformItem.SetParent(parent.transform);
        ItemWorld itemWorld = transformItem.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        return itemWorld;
    }

    public void SetItem(Item item)
    {
        this.item = item;
    }

    /**
     * Spawns item in front of player.
     */
    public static ItemWorld DropItem(Transform playerTransform, Item item)
    {
        Vector3 playerPos = playerTransform.position;
        Vector3 playerDirection = playerTransform.forward;
        Quaternion playerRotation = playerTransform.rotation;
        float spawnDistance = 2;
        Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

        ItemWorld itemWorld = SpawnItemWorld(spawnPos, playerRotation, item);
        return itemWorld;
    }

    public Item GetItem()
    {
        return item;
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
