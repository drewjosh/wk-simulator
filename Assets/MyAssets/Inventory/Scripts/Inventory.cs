using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    /** Equipment **/
    public Item ammunition;
    public List<Item> clothingItems;
    public ArrayList equipmentItemList; // items can be carried on person
    public int equipmentItemListMaxSize = 10;
    public ArrayList luggageItems; // holds luggage items like karusa and tarusa used for ui
    public ArrayList inventoryExtensions; // holds inventory extensions used for inventory
    private CharacterAssets characterAssets;

    // event to notify itemlist changed
    public event EventHandler OnItemListChanged;

    public Inventory()
    {
        // init personal items
        equipmentItemList = new ArrayList(equipmentItemListMaxSize); // TODO in the future we have to load this from storage
        equipmentItemList = ArrayListUtils.FillArrayList(equipmentItemListMaxSize);

        // init clothing
        clothingItems = new List<Item>();
        characterAssets = GameObject.Find("CharacterAssets").GetComponent<CharacterAssets>();

        // init luggage items
        luggageItems = new ArrayList
        {
            new Item(Item.ItemType.karusa),
            new Item(Item.ItemType.tarusa),
            new Item(Item.ItemType.ftasche),
            new Item(Item.ItemType.rolli),
            new Item(Item.ItemType.gte)
        };

        // init inventory extensions
        inventoryExtensions = new ArrayList
        {
            new InventoryExtension(Item.ItemType.karusa),
            new InventoryExtension(Item.ItemType.gte),
            new InventoryExtension(Item.ItemType.tarusa),
            new InventoryExtension(Item.ItemType.ftasche),
            new InventoryExtension(Item.ItemType.rolli)
        };

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    /**
     * Adds item to inventory.
     * We can only pick up items into equipment items.
     */
    public bool AddItem(Item item)
    {
        if (equipmentItemList.Count < equipmentItemListMaxSize)
        {
            for (int i = 0; i < equipmentItemList.Count; i++)
            {
                if (equipmentItemList[i] == null)
                {
                    // add a duplicate item to first free slot
                    equipmentItemList[i] = new Item(item.itemType);
                    OnItemListChanged?.Invoke(this, EventArgs.Empty);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns inventory by extension type. 
     */
    public InventoryExtension GetInventoryExtensionByItemType(Item.ItemType type)
    {
        foreach (InventoryExtension inventoryExtension in inventoryExtensions)
        {
            if (inventoryExtension != null && type == inventoryExtension.type)
            {
                return inventoryExtension;
            }
        }
        return null;
    }

    public void HandleNewItemOrder(Item item, int newSlotIndex, ArrayList newParentList)
    {
        if (item.IsClothing() && newSlotIndex == -1 && item.parentList != null)
        {
            // clothing item was moved to clothing list, item comes from another list
            SetClothingItem(item.itemSlotType, item, false);

            item.parentList[item.currentSlotIndex] = null;
            item.parentList = null;
        }
        else if (item.IsClothing() && newSlotIndex != -1 && item.parentList == null)
        {
            // clothing item was moved away from clothing list, add there
            SetClothingItem(item.itemSlotType, item, true);
            newParentList[newSlotIndex] = item;
            item.parentList = newParentList;
        }
        else
        {
            // remove item from old list
            item.parentList[item.currentSlotIndex] = null;

            // add item to new slot
            newParentList[newSlotIndex] = item;
        }

        // TODO is clothing item rendered?
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    /**
     * Sets item according to item slot type.
     * isRemoval true when item was removed away from clothing.
     */
    private void SetClothingItem(Item.ItemSlotType itemSlotType, Item item, bool isRemoval)
    {
        if (isRemoval)
        {
            // remove clothing from character
            if (item.itemType == Item.ItemType.trouserTenueB)
            {
                characterAssets.trouser_tenue_b.SetActive(false);
            }

            // search item and delete
            for (int i = 0; i < clothingItems.Count; i++)
            {
                if (clothingItems[i] != null && clothingItems[i].itemSlotType == itemSlotType)
                {
                    clothingItems[i] = null;
                }
            }
        }
        else
        {
            // add clothing to character
            if (item.itemType == Item.ItemType.trouserTenueB)
            {
                characterAssets.trouser_tenue_b.SetActive(true);
            }
            clothingItems.Add(item);
        }
    }

    /**
     * Add item to a inventory extension.
     */
    public void AddItemToInventoryExtension(Item.ItemType destinationType, Item item)
    {
        if (destinationType == Item.ItemType.rolli)
        {
            GetInventoryExtensionByItemType(destinationType).AddItemToFirstPossible(item);
            OnItemListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
