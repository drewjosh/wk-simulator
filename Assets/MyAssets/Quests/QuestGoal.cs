using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class QuestGoal : MonoBehaviour
{
    public GoalType goalType;
    public Quest quest; // cross reference to access quest

    public int requiredAmount;
    public int currentAmount;

    // Finding items
    public List<Item> requiredToFind;
    public List<Item> leftToFind;
    public List<Item> currentlyFound;
    public float timeLimit; // in sec
    private float timeRemaining;
    public bool timerIsRunning = false;
    public List<Transform> spawnPoints;
    public float spawningDistance = 15f;
    public GameObject spawningParent; // hold spawning objects to destroy on reset

    // finding a location
    // QuestGoal.cs should be attached to GameObject with Collider Trigger of destination
    private bool destinationReached = false;

    private Quest_UI_Controller questUIController;

    private void Awake()
    {
        questUIController = GameObject.Find("Quest_UI_Controller").GetComponent<Quest_UI_Controller>();

        if (goalType == GoalType.FindingItems)
        {
            // deactivate game objects used for spawn points
            foreach (Transform spawnPoint in spawnPoints)
            {
                spawnPoint.gameObject.SetActive(false);
            }
        }
    }

    public enum GoalType
    {
        FindingItems, // for example searching items
        Rescue,
        Covid19Test,
        FindingLocation
    }

    public bool IsReached()
    {
        if (goalType == GoalType.FindingItems)
        {
            if (leftToFind.Count <= 0 && timeRemaining > 0)
            {
                // all items were found and we still have time
                questUIController.ShowCountdown(false);
                questUIController.ShowQuestResult(true, true);
                return true;
            }
        }
        else if (goalType == GoalType.FindingLocation)
        {
            return destinationReached;
        }
        return false;
    }

    public void StartQuest()
    {
        if (goalType == GoalType.FindingItems)
        {
            timeRemaining = timeLimit;
            timerIsRunning = true;
            leftToFind = new List<Item>();
            foreach (Item item in new List<Item>(requiredToFind))
            {
                leftToFind.Add(new Item(item.itemType));
            }
            requiredToFind = new List<Item>(leftToFind);
            SpawnObjects();
            questUIController.ShowCountdown(true);
        }
        else if (goalType == GoalType.FindingLocation)
        {
            destinationReached = false;
        }
    }

    private void SpawnObjects()
    {
        for (int i = 0; i < requiredToFind.Count; i++)
        {
            Vector3 randomDirection = Random.insideUnitSphere * spawningDistance;
            randomDirection += spawnPoints[i].position;
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, spawningDistance, 1);
            Vector3 finalPosition = hit.position;
            ItemWorld.SpawnItemWorldToParent(finalPosition, Quaternion.identity, requiredToFind[i], spawningParent);
        }
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                questUIController.SetCountdownTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                questUIController.ShowQuestResult(false, true);
                questUIController.ShowCountdown(false);
                quest.isActive = false;
                ResetGoal();
            }
        }
    }

    public void ItemCollected(Item collectedItem)
    {
        if (goalType == GoalType.FindingItems)
        {
            Debug.Log("We found an item!");
            for (int i = 0; i < leftToFind.Count; i++)
            {
                if (collectedItem.itemType == leftToFind[i].itemType)
                {
                    // we found a required item
                    Debug.Log("We found: " + collectedItem.itemType);
                    currentlyFound.Add(collectedItem);
                    leftToFind.RemoveAt(i);
                    break; // jump out of loop
                }
            }
        }
    }

    private void ResetGoal()
    {
        foreach (Transform child in spawningParent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player" && goalType == GoalType.FindingLocation)
        {
            destinationReached = true;
            Debug.Log("Destination reached!");
        }
    }
}