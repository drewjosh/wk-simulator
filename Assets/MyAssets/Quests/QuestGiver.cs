using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestGiver : MonoBehaviour
{
    public Quest quest;

    public Player player;

    private Quest_UI_Controller questUIController;

    private void Awake()
    {
        questUIController = GameObject.Find("Quest_UI_Controller").GetComponent<Quest_UI_Controller>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player" && !quest.isActive)
        {
            questUIController.OpenQuestWindow(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player" && !quest.isActive)
        {
            // simply close the quest window when exit area of quest
            questUIController.DeclineQuestOnClick();
        }
    }

    public void StartQuest()
    {
        Debug.Log("Starting quest: " + quest.title);
        quest.isActive = true;
        player.currentQuest = quest;
        quest.StartQuest();
        quest.goal.StartQuest();
    }
}
