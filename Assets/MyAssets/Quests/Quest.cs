using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Quest : MonoBehaviour
{
    public bool isActive;

    // meta
    public string title;
    public string description;

    // reward for completing quest
    public int experienceReward;
    public int moneyReward;

    // soundtrack to play during quest
    public AudioClip soundtrack;

    // describbes goal of the quest
    public QuestGoal goal;

    private SoundtrackController soundtrackController;

    private void Awake()
    {
        soundtrackController = GameObject.Find("SoundtrackController").GetComponent<SoundtrackController>();
    }

    public void Complete()
    {
        soundtrackController.StopCurrentSoundtrack();
        isActive = false;
        Debug.Log("Quest " + title + " completed!");
    }

    public void StartQuest()
    {
        Debug.Log("Quest " + title + " started.");
        if (soundtrack != null)
        {
            soundtrackController.PlaySoundtrack(soundtrack);
        }
    }
}
