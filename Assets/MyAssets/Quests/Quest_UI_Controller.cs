using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

/**
 * Holds and handles all UI for quests.
 */
public class Quest_UI_Controller : MonoBehaviour
{
    public GameObject questWindow;
    public TextMeshProUGUI title;
    public TextMeshProUGUI description;
    public TextMeshProUGUI timeLimit;
    public GameObject timeLabel;
    public GameObject FindingList;

    // items for finding
    public Transform findingListItemSlotContainer;
    private Transform itemSlotTemplate;

    // quest giver of quest that currently is displayed/handled in UI
    private QuestGiver currentQuestGiver;

    // quest result window
    // fail and success
    public GameObject successWindow;
    public GameObject failWindow;
    private TextMeshProUGUI successMissionTitle;
    private TextMeshProUGUI failMissionTitle;

    // countdown
    public GameObject countdownObject;
    private TextMeshProUGUI countdownText;

    private void Awake()
    {
        itemSlotTemplate = findingListItemSlotContainer.Find("ItemSlotTemplate");
        questWindow.GetComponent<CanvasGroup>().alpha = 0;
        questWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;

        // init quest result
        successMissionTitle = successWindow.transform.Find("MissionTitle").GetComponent<TextMeshProUGUI>();
        failMissionTitle = failWindow.transform.Find("MissionTitle").GetComponent<TextMeshProUGUI>();

        // init countdown
        countdownText = countdownObject.transform.Find("timerText").GetComponent<TextMeshProUGUI>();
        countdownObject.GetComponent<CanvasGroup>().alpha = 0;
        countdownObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    /**
     * Opens quest window and sets all info about quest.
     */
    public void OpenQuestWindow(QuestGiver questGiver)
    {
        currentQuestGiver = questGiver;
        questWindow.GetComponent<CanvasGroup>().alpha = 1;
        questWindow.GetComponent<CanvasGroup>().blocksRaycasts = true;
        title.text = questGiver.quest.title;
        description.text = questGiver.quest.description;
        if (questGiver.quest.goal.goalType == QuestGoal.GoalType.FindingItems)
        {
            FindingList.SetActive(true);
            RenderFindingItems(questGiver.quest);
            timeLabel.SetActive(true);
            timeLimit.text = questGiver.quest.goal.timeLimit / 60 + " min";
        }
        else
        {
            timeLabel.SetActive(false);
            timeLimit.text = "";
            FindingList.SetActive(false);
        }
    }

    /**
     * User clicked to accept quest in UI.
     */
    public void AcceptQuestOnClick()
    {
        // set result window titles to have it ready for later
        successMissionTitle.text = currentQuestGiver.quest.title;
        failMissionTitle.text = currentQuestGiver.quest.title;

        questWindow.GetComponent<CanvasGroup>().alpha = 0;
        questWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
        currentQuestGiver.StartQuest();
    }

    /**
     * User clicked to decline quest in UI.
     */
    public void DeclineQuestOnClick()
    {
        questWindow.GetComponent<CanvasGroup>().alpha = 0;
        questWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    /**
     * Renders items to find in quest window.
     */
    private void RenderFindingItems(Quest quest)
    {
        // draw all transforms in item list
        int x = 0;
        int y = 0;
        float itemSlotCellSize = 150f;
        foreach (Item item in quest.goal.requiredToFind)
        {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, findingListItemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.sprite;

            // show items in a grid
            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, y * itemSlotCellSize * -1);
            x++;
            if (x > 5)
            {
                x = 0;
                y++;
            }
        }
    }

    /**
     * Closes quest result window.
     */
    public void QuestResultOkOnClick()
    {
        successWindow.SetActive(false);
        failWindow.SetActive(false);
    }

    /**
     * Shows and hides countdown.
     */
    public void ShowCountdown(bool isShown)
    {
        countdownObject.GetComponent<CanvasGroup>().alpha = isShown ? 1 : 0;
        countdownObject.GetComponent<CanvasGroup>().blocksRaycasts = isShown;
    }

    /**
     * Show quest result window for quest success or fail.
     */
    public void ShowQuestResult(bool isSuccess, bool isShown)
    {
        if (isSuccess)
        {
            successWindow.GetComponent<CanvasGroup>().alpha = isShown ? 1 : 0;
            successWindow.GetComponent<CanvasGroup>().blocksRaycasts = isShown;
        }
        else
        {
            failWindow.GetComponent<CanvasGroup>().alpha = isShown ? 1 : 0;
            failWindow.GetComponent<CanvasGroup>().blocksRaycasts = isShown;
        }
    }

    /**
     * Sets time of countdown.
     */
    public void SetCountdownTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        countdownText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
